# Accolades CLI

## About

A client application and/or library, usable for automating purposes like pushing of badges (and their related awarding conditions) to the Collection via the Accolades API, and other such high-level interactions offered by the Accolades API

## Read more

1. https://fedora-arc.readthedocs.io/en/latest/badges/prop_rewrite_entities.html#internal-entities
